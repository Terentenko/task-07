package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
        User f = new User();
		f.setLogin(login);
		f.setId(0);
		return f;
	}
	@Override
	public boolean equals(Object obj){


		return Objects.equals(obj.toString(), this.login);
	}

	@Override
	public String toString() {
		return login ;
	}
}
