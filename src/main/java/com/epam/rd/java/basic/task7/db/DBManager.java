package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	//Connection con =null;
private static String uriTest;

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}
	private DBManager() {

		try (InputStream input = new FileInputStream("app.properties")) {
			Properties prop = new Properties();

			prop.load(input);
						//connection.url=jdbc:mysql://localhost:3306/testdb?user=root&password=123456789
			//uriTest = "jdbc:mysql://localhost:3306/testdb?user=root&password=123456789";
			 uriTest =prop.getProperty("connection.url");
			// connection to db

		//	Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();


		} catch (IOException  ex) {
			ex.printStackTrace();

		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> list = new LinkedList<>();
		//Statement stmt = null;
		try(Connection con = DriverManager.getConnection(uriTest)) {
			//stmt=  con.prepareStatement("SELECT * FROM users");
			ResultSet resultSet = con.createStatement().executeQuery("SELECT * FROM users");

			while(resultSet.next()){
				User user=new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));

				list.add(user);
//System.out.println(list);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(" LYPONELO :::findAllUsers",e);
		}


		return list;
	}

	public boolean insertUser(User user) throws DBException {
		if(user==null) return false;
Statement sat =null;
		try (Connection con = DriverManager.getConnection(uriTest)) {

			sat=con.createStatement();
			if(sat.executeUpdate("INSERT INTO  users (login) VALUES ('"+user.getLogin()+"')",Statement.RETURN_GENERATED_KEYS)>0) {

				ResultSet resultSet = sat.getGeneratedKeys();

				while (resultSet.next()) {
					user.setId(resultSet.getInt(1));
					//System.out.println(resultSet.getInt(1));
				}
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("LUPONYLO :::insertUser",e);

		}

		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if(users.length==0) return false;
		Statement sat = null;
		try (Connection con = DriverManager.getConnection(uriTest)){
			sat=con.createStatement();
			for(User y :users) {
				if(y!=null) sat.executeUpdate("DELETE FROM users WHERE login ='"+y.getLogin()+"'");
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("LUPONYLO :::deleteUsers",e);
		}



	}
		public User getUser(String login) throws DBException {
		if(login==null) return null;

			PreparedStatement stmt = null;
			try(Connection con = DriverManager.getConnection(uriTest)){
				stmt=  con.prepareStatement("SELECT * FROM users WHERE login=?");
				stmt.setString(1, login);

				ResultSet resultSet = stmt.executeQuery();

				while(resultSet.next()){
					User user=new User();
					user.setLogin(resultSet.getString(2));
					user.setId(resultSet.getInt(1));
					return user;


				}

			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException(" LYPONELO :::getUser",e);
			}
			return null;

	}

	public Team getTeam(String name) throws DBException {
		if(name==null)return null;

		PreparedStatement stmt = null;
		try(Connection con = DriverManager.getConnection(uriTest)) {
			stmt=  con.prepareStatement("SELECT * FROM teams WHERE name=?");
			stmt.setString(1, name);

			ResultSet resultSet = stmt.executeQuery();

			while(resultSet.next()){
				Team team=new Team();
				team.setName(resultSet.getString(2));
				team.setId(resultSet.getInt(1));
				return team;


			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(" LYPONELO :::getTeam",e);
		}
		return null;



	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> list = new LinkedList<>();
		PreparedStatement stmt = null;
		try(Connection con = DriverManager.getConnection(uriTest)) {
			//stmt=  con.prepareStatement("SELECT * FROM teams");
			ResultSet resultSet = con.createStatement().executeQuery("SELECT * FROM teams");
			Team team;
			while(resultSet.next()){
				team=new Team();
				team.setName(resultSet.getString(2));
				team.setId(resultSet.getInt(1));
				list.add(team);
//System.out.println(list);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(" LYPONELO :::findAllTeams",e);
		}


		return list;
	}

	public boolean insertTeam(Team team) throws DBException {
		if(team==null)return false;
		Statement sat =null;
		try(Connection con = DriverManager.getConnection(uriTest)) {

			sat=con.createStatement();
			if(1==sat.executeUpdate("INSERT INTO  teams (name) VALUES ('"+team.getName()+"')",Statement.RETURN_GENERATED_KEYS)) {
				ResultSet resultSet = sat.getGeneratedKeys();

				while (resultSet.next()) {
					team.setId(resultSet.getInt(1));
					//System.out.println(resultSet.getInt(1));
				}
				return true;
			}




		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(" LYPONELO :::insertTeam",e);
		}

		return false;
	}


	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if(user==null||teams.length==0) return false;
		Connection con=null;
		PreparedStatement prs= null;
		try {
			con =DriverManager.getConnection(uriTest);
			con.setAutoCommit(false);
			prs= con.prepareStatement("INSERT  INTO users_teams (user_id,team_id) VALUES ("+user.getId()+",?)");
			boolean flag =false;
			for(Team t:teams){
				if(t!=null){
					prs.setInt(1,t.getId());
					flag = (prs.executeUpdate()>0||flag);

				}
			}
			con.commit();
			return flag;


		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);

			throw new DBException(" LYPONELO :::insertTeam",e);


		}
		finally {
			close(prs);
			close(con);

		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		if(user==null) return null;
		List<Team> list = new LinkedList<>();
		PreparedStatement stmt = null;
		try (Connection con = DriverManager.getConnection(uriTest)){
			stmt=  con.prepareStatement("SELECT team_id,name FROM users_teams JOIN teams ON team_id = id WHERE user_id = ?");
			stmt.setInt(1, user.getId());

			ResultSet resultSet = stmt.executeQuery();

			while(resultSet.next()) {
				Team team = new Team();
				team.setName(resultSet.getString(2));
				team.setId(resultSet.getInt(1));
				list.add(team);
							}
			return list;





		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(" LYPONELO :::getUserTeams",e);
		}
		//return list;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if(team==null) return false;
		Statement sat = null;
		try(Connection con = DriverManager.getConnection(uriTest)) {
			sat=con.createStatement();

				if(1==sat.executeUpdate("DELETE FROM teams WHERE name ='"+team.getName()+"'")) return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("LUPONYLO :::deleteUsers",e);
	}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		if(team==null) return false;
		Statement sat = null;
		try(Connection con = DriverManager.getConnection(uriTest)) {
			sat=con.createStatement();

			if(1==sat.executeUpdate("UPDATE teams SET name = '"+team.getName()+"' WHERE id = "+team.getId())) return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("LUPONYLO :::deleteUsers",e);
		}
		return false;
	}









	private void rollback(Connection conx) {
		if (conx != null) {
			try {
				conx.rollback();
			} catch (SQLException ex) {

			}
		}
	}
	private void close(PreparedStatement s) {
		if (s != null) {
			try {
				s.close();
			} catch (SQLException ex) {

			}
		}
	}
	private void close(Connection co) {
		if (co != null) {
			try {
				co.setAutoCommit(true);
				co.close();
			} catch (SQLException ex) {

			}
		}
	}

}
